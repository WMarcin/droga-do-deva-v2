package com.kitopi.api.study.application.Computer;

import java.util.ArrayList;
import java.util.List;

public class Order {

    private final List<Item> items = new ArrayList<>();

    private Order() {
    }

    public static Order of() {
        return new Order();
    }


    public void addItem(Item item) {
        if (this.items.size() >= 5) {
            throw new RuntimeException("Too many items");
        }
        this.items.add(item);
    }

    public double totalPrice() {
        double fullPrice = calculatePrice();
        if (fullPrice >= 3500) {
            return fullPrice- fullPrice * 10 / 100;
        } else {
            return fullPrice;
        }
    }

    public double calculatePrice() {
        return this.items.stream().mapToDouble(Item::getPrice).sum();
    }

}
