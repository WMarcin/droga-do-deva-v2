package com.kitopi.api.study.application.Car;

public class Car {

    private final int engineCapacity;

    private int currentQuantity;

    private boolean isDrive;

    public Car(int engineCapacity) {
        this.engineCapacity = engineCapacity;
        this.currentQuantity = 0;
        this.isDrive = false;
    }

    public boolean isDrive() {
        return isDrive;
    }

    public int getCurrentQuantity() {
        return currentQuantity;
    }
    public void addFuel(int fuelToAdd) {
        int fuelAfterAdding = currentQuantity += fuelToAdd;

        if(this.engineCapacity < fuelAfterAdding) {
            throw new RuntimeException();
        }
    }


}
