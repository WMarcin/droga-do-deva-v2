package com.kitopi.api.study.application.Message;

import java.util.Date;

public class Message {

    final private String message;

    final private String recipient;

    final private CurrentDateProvider dateProvider;

    final private Date date;

    public Date getDate() {
        return date;
    }

    public String getMessage() {
        return message;
    }

    public Message setMessage(String message) {
        Message fullMessage = Message.fullMessage(message, getRecipient(), dateProvider);
        return fullMessage;
    }
    public Message setRecipient(String recipient) {
        Message fullMessage = Message.fullMessage(getMessage(), recipient, dateProvider);
        return fullMessage;
    }

    public String getRecipient() {
        return recipient;
    }

    private Message(String message, String recipient, CurrentDateProvider dateProvider) {
        this.message = message;
        this.recipient = recipient;
        this.dateProvider = dateProvider;
        this.date = dateProvider.now();
    }

    public static Message fullMessage(String message, String recipient, CurrentDateProvider dateProvider) {
         Message fullMessage = new Message(message, recipient, dateProvider);
        return fullMessage;
    }
}

