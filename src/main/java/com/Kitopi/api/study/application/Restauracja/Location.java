package com.kitopi.api.study.application.Restauracja;

public class Location {

    private final double x;
    private final double y;

    public Location (double x, double y){
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "Location{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
