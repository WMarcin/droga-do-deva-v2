package com.kitopi.api.study.application.Message;

import java.util.Date;

public class CurrentDateForTest implements CurrentDateProvider {

    private Date currentDate = new Date();

    @Override
    public Date now() {
        return currentDate;
    }

    public void setCurrentDate(Date currentDate) {
        this.currentDate = currentDate;
    }
}
