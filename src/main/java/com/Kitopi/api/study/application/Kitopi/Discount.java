package com.kitopi.api.study.application.Kitopi;


interface Discount {

   double getTotalPrice(double price);
}

