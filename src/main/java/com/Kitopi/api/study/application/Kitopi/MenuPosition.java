package com.kitopi.api.study.application.Kitopi;

public class MenuPosition {

    private final String name;

    private final double price;


    public MenuPosition(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public double getPrice() {
        return price;
    }


}
