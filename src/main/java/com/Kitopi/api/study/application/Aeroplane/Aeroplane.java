package com.kitopi.api.study.application.Aeroplane;

public class Aeroplane {

    private final int fuelCapacity;

    private int currentFuel;

    private boolean isFlying;

    public Aeroplane(int fuelCapacity) {
        this.fuelCapacity = fuelCapacity;
        this.currentFuel = 0;
        this.isFlying = false;
    }

    public boolean isFlying() {
        return isFlying;
    }

    public int getCurrentFuel() {
        return currentFuel;
    }

    public void addFuel(int fuelToAdd) {
        int fuelAfterAdding = this.currentFuel + fuelToAdd;

        if(this.fuelCapacity < fuelAfterAdding) {
            throw new RuntimeException(String.format("Fuel level cannot be exceeded above %d", fuelCapacity));
        }

        this.currentFuel = fuelAfterAdding;
    }

    public void takeOff() {
        if(currentFuel < 10) {
            throw new RuntimeException("Cannot take off - not enought fuel");
        }

        if(isFlying) {
            throw new RuntimeException("Plane is already flying");
        }

        isFlying = true;
    }

    public void land() {
        if(isFlying == false) {
            throw new RuntimeException("Plane is already on the ground");
        }

        isFlying = false;
    }
    public void fly(int minutes ) {
        if(isFlying ==false){
            throw new RuntimeException("Plane is not able to fly, not started yet");
        }
        int fuelCost = minutes * 10;
        if(currentFuel - fuelCost < 0 ) {
            throw new RuntimeException("Not enough fuel to reach your destination");
        }
        this.currentFuel -= fuelCost;
    }



}
