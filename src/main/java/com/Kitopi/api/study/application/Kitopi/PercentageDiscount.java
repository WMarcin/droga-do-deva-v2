package com.kitopi.api.study.application.Kitopi;

public class PercentageDiscount implements Discount {

    private int discount;

    public PercentageDiscount(int discount) {
        this.discount = discount;
    }

    @Override
    public double getTotalPrice(double price) {
        return price - price*discount/100;
    }

}
