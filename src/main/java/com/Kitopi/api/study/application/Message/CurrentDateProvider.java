package com.kitopi.api.study.application.Message;

import java.util.Date;

public interface CurrentDateProvider {

    public Date now();
}
