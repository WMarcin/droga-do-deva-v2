package com.kitopi.api.study.application.Laser;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class Laser {

    public int loadingLaser(int loadTime) throws InterruptedException {
//        try {
//            Optional<String> laser = Optional.of(null);
//            laser.map(String::toUpperCase).orElseThrow(new RuntimeException);
            TimeUnit.SECONDS.sleep(loadTime);
            if (loadTime >= 10) {
                return 100;
            }
            return loadTime * 10;
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }
    }
}
