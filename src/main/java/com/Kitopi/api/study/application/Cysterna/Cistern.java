package com.kitopi.api.study.application.Cysterna;

import static com.kitopi.api.study.application.Cysterna.Liquid.FUEL;
import static com.kitopi.api.study.application.Cysterna.Liquid.VODKA;

public class Cistern {

    private Liquid liquid;
    private double maxCapacity;
    private double capacity;

    public Cistern(Liquid liquid, double maxCapacity) {
        this.liquid = liquid;
        this.maxCapacity = maxCapacity;
    }

    public double getCapacity() {
        return capacity;
    }

    public Liquid getLiquid() {
        return liquid;
    }

    public double getMaxCapacity() {
        return maxCapacity;
    }

    public String toString() {
        return "Cysterna o pojemności " + getMaxCapacity() + " " + "litrów " + "przechowuje " + getCapacity() + " " + "litrów płynu typu " + getLiquid();
    }

    public double pourIn(double quantity) {
        if (this.getLiquid().equals(VODKA)) {
            System.out.println("Nie można wlewać");
        } else if (this.capacity + quantity <= maxCapacity) {
            this.capacity += quantity;
        } else
            System.out.println("Brak miejsca w cysternie");
        return quantity;
    }

    public double pourOut(double quantity) {
        if (quantity <= this.capacity) {
            this.capacity -= quantity;
        }
        if (this.capacity == 0) {
            System.out.println("Możesz wlać inne paliwo");
            if (this.getLiquid().equals(FUEL)) {
                {
                    System.out.println("Napełnij");
                }
            } else
                System.out.println("Nie ma tyle płynu w cysternie");
            return quantity;
        }
        return quantity;
    }
}