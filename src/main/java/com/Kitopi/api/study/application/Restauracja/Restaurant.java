package com.kitopi.api.study.application.Restauracja;

import java.time.LocalTime;

public class Restaurant {

    private final Location location;

    private final LocalTime open;

    private final LocalTime close;

    private final boolean onSpot;

    private final boolean delivery;

    public Restaurant(Location location, LocalTime open, LocalTime close, boolean onSpot, boolean delivery) {
        this.location = location;
        this.open = open;
        this.close = close;
        this.onSpot = onSpot;
        this.delivery = delivery;
    }

    public boolean isOnSpot() {
        return onSpot;
    }

    public Location getLocation() {
        return location;
    }

    public LocalTime getOpen() {
        return open;
    }

    public LocalTime getClose() {
        return close;
    }

    public boolean isRestaurantOpen(LocalTime localTime) {
        return localTime.isBefore(close) && localTime.isAfter(open);
    }

    @Override
    public String toString() {
        return "Restaurant{" +
                "location=" + location +
                ", open=" + open +
                ", close=" + close +
                ", onSpot=" + onSpot +
                ", delivery=" + delivery +
                '}';
    }
}

