package com.kitopi.api.study.application.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;

public class Exam {

    private final List<Grade> gradeList = new ArrayList<>();

    private Exam() {
    }

    public static Exam of() {
        return new Exam();
    }

    public void addGrade(Grade grade) {
        long sameStudentGradeCount = gradeList.stream()
                .filter(filteredGrade -> filteredGrade.getStudentId().equals(grade.getStudentId()))
                .count();

        if(sameStudentGradeCount > 0) {
            throw new RuntimeException("a student may only have one grade");
        }

//        if (gradeList.contains(grade)) {
//            throw new RuntimeException("a student may only have one grade");
//        }
        this.gradeList.add(grade);
    }

    private double sumGrade() {
        return this.gradeList.stream().mapToDouble(Grade::getGrade).sum();
    }

    public double averageGrade() {
        return this.sumGrade() / this.gradeList.size();
    }

    public double averageGrade2() {
        OptionalDouble optionalAverage = this.gradeList.stream().mapToDouble(Grade::getGrade).average();

        if(optionalAverage.isPresent()) {
            return optionalAverage.getAsDouble();
        } else {
            return 0.0;
        }
    }

    public List<Grade> failedExam() {
        return  this.gradeList.stream()
                .filter(grade -> grade.getGrade()==(1))
                .toList();
    }
}

