package com.kitopi.api.study.application.Kitopi;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Order {

    private final List<MenuPosition> menuPositions = new ArrayList<>();

    private Order() {
    }

    public static Order of() {
        return new Order();
    }

    public void addMenuPosition(MenuPosition menuPosition) {
        double totalPrice = calculatePrice() + menuPosition.getPrice();
        if (this.menuPositions.size() >= 10) {
            throw new RuntimeException("Too many items");
        }
        if (totalPrice > 15) {
            throw new RuntimeException("Maximum price exceeded");
        }
        this.menuPositions.add(menuPosition);
    }

    public double calculatePrice(Discount discount) {
       return discount.getTotalPrice(calculatePrice());
    }
    public double calculatePrice (){
        return this.menuPositions.stream().mapToDouble(MenuPosition::getPrice).sum();
    }

}

