package com.kitopi.api.study.application.Kitopi;

public class Percentage30Discount implements Discount{

    @Override
    public double getTotalPrice(double price) {
        return price - price*30/100;
    }
}
