package com.kitopi.api.study.application.Message;

import java.util.Date;

public class CurrentDate implements CurrentDateProvider {

    @Override
    public Date now() {
        return new Date();
    }
}
