package com.kitopi.api.study.application.Kitopi;

public class QuantitativeDiscount implements Discount{

    @Override
    public double getTotalPrice(double price) {
        return price-10;
    }
}
