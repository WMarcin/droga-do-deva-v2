package com.kitopi.api.study.application.Kitopi;

public class Percentage60Discount implements Discount{

    @Override
    public double getTotalPrice(double price) {
        return  price - price*60/100;
    }
}
