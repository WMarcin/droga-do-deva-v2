package com.kitopi.api.study.application.Cysterna;

import org.springframework.stereotype.Service;

@Service
public class CisternsServices {

    public double pouring(Cistern cistern) {
        cistern.pourIn(100);
        return cistern.getCapacity();
    }


}
