package com.kitopi.api.study.application.Restauracja;

import java.util.ArrayList;
import java.util.List;

public class Brand {

    private final String name;
    private final boolean onSpot;
    private final boolean delivery;

    private List<Restaurant> restaurants = new ArrayList<>();

    public Brand (String name, boolean onSpot, boolean delivery){
        this.name = name;
        this.onSpot = onSpot;
        this.delivery = delivery;
    }
    public Brand addRestaurant (Restaurant restaurant){
        restaurants.add(restaurant);
        return this;
    }
    public List<Restaurant> getOnSpotResturants (){
        return restaurants.stream()
                .filter(restaurant -> restaurant.isOnSpot())
                .toList();
    }

    @Override
    public String toString() {
        return "Brand{" +
                "name='" + name + '\'' +
                ", onSpot=" + onSpot +
                ", delivery=" + delivery +
                ", restaurants=" + restaurants +
                '}';
    }
}

