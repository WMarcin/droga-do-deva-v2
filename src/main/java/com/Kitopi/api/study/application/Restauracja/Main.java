package com.kitopi.api.study.application.Restauracja;

import java.time.LocalTime;

public class Main {

    public static void main(String [] args){

        Location location = new Location(1.0,2.2);
        LocalTime from = LocalTime.of(12,20);
        LocalTime to = LocalTime.of(20,25);
        Restaurant restaurant1 = new Restaurant(location, from, to,true,true);
        Restaurant restaurant2 = new Restaurant(location, from, to,false,true);

        Brand brand1 = new Brand("MC",true,true);
        Brand brand2 = new Brand("KFC",false,false);
        Brand brand3 = new Brand("Pizza",true,false);


       brand1.addRestaurant(restaurant1).addRestaurant(restaurant2);
       System.out.println(brand1.getOnSpotResturants());

    }
}
