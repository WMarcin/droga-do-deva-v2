package com.kitopi.api.study.application.immutability;

public class StarDestroyer {

    final private int currentPower;

    final private String name;

    public StarDestroyer(String name) {
        this.name = name;
        this.currentPower = 0;
    }

    private StarDestroyer(int startingPower, String name) {
        this.name = name;
        currentPower = startingPower;
    }

    public int getCurrentPower() {
        return currentPower;
    }

    public String getName() {
        return name;
    }

    public StarDestroyer loadLaser(int loadTime) {
        StarDestroyer newDestroyer = new StarDestroyer(getCurrentPower() + loadTime*10, getName());

        return newDestroyer;
    }

    public StarDestroyer fire() {
        return new StarDestroyer(getName());
    }
}
