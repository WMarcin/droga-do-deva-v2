package com.kitopi.api.study.application.Hotel;

public class Hotel {

    private final String name;

    private final double price;

    public Hotel(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public double getPrice() {
        return price;
    }
}
