package com.kitopi.api.study.application.Student;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.util.Objects;

public class Grade {

    @Pattern(regexp = "^[0-9]{6}$")
    @Valid
    private final String studentId;

    @Pattern(regexp = "^[1-6]")
    @Valid
    private final double grade;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Grade grade1 = (Grade) o;

//        return
//                grade == grade1.grade &&
//                Objects.equals(studentId, grade1.studentId);
        return Double.compare(grade1.grade, grade) == 0 && studentId.equals(grade1.studentId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(studentId, grade);
    }

    public String getStudentId() {
        return studentId;
    }

    private Grade(String studentId, double grade) {
        this.studentId = studentId;
        this.grade = grade;
    }

    public static Grade of(String studentId, double grade) {
        //validation


        return new Grade(studentId, grade);
    }

    public double getGrade() {
        return grade;
    }
}
