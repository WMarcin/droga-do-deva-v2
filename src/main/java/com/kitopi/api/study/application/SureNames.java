package com.kitopi.api.study.application;

import org.springframework.stereotype.Service;

@Service
public class SureNames {

    public String getFullName(String name, String sureName) {
        return name + " " + sureName;
    }

}
