package com.kitopi.api.study.boundary;

import io.swagger.annotations.ApiModelProperty;

record FullNameDocument(@ApiModelProperty(required = true, value = "First name") String name,
                        @ApiModelProperty(required = true, value = "Second name") String secondName) {

}
