package com.kitopi.api.study.boundary;

import io.swagger.annotations.ApiOperation;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import com.kitopi.api.study.application.SureNames;


import static org.springframework.http.HttpStatus.NO_CONTENT;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class FullNameController {

    private final SureNames sureNames;

    @GetMapping("/getFullName")
    String getString() {
        List<String> pętla= new java.util.ArrayList<>(List.of());
        String string1 = "Marcin";
        String string2 = "Wrzeszcz";
        pętla.add(string1);
        pętla.add(string2);
        pętla.forEach(System.out::println);
        String result = string1 + " " + string2;
        return result;
    }

    @ApiOperation(value = "Get product details")
    @PutMapping("/strings")
    @ResponseStatus(NO_CONTENT)
    void putString(@RequestBody FullNameDocument fullNameDocument) {
        String fullName = sureNames.getFullName(fullNameDocument.name(), fullNameDocument.secondName());
        System.out.println(fullName);

   }

    @ApiOperation(value = "Get product details")
    @PostMapping("/strings")
    @ResponseStatus(NO_CONTENT)
    void postString(@RequestBody FullNameDocument cokolwiek) {
    }

}
