package com.kitopi.api.study

import com.kitopi.api.study.application.Cysterna.Cistern
import com.kitopi.api.study.application.Cysterna.CisternsServices
import com.kitopi.api.study.application.Cysterna.Liquid
import spock.lang.Specification

class CisternsServicesSpec extends Specification{

    CisternsServices cisternsServices = new CisternsServices()

    def 'increase the tank content by one hundred' (){
        given:
            Cistern cistern = new Cistern(Liquid.FUEL,2000)
        when:
            double result = pouringLiquid(cistern)
        then:
            result == 100
    }


    private double pouringLiquid(Cistern cistern) {
        return cisternsServices.pouring(cistern)
    }
}
