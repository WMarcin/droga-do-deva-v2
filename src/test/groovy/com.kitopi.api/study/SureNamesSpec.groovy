package com.kitopi.api.study

import com.kitopi.api.study.application.SureNames
import spock.lang.Specification

class SureNamesSpec extends Specification {

    SureNames sureNames = new SureNames()

    def 'should return full name'() {
        given:
            String firstName = "Marcin"
            String sureName = "Krzyzowski"
        when:
            String fullName = sureNames.getFullName(firstName, sureName)
        then:
            fullName == firstName + " " + sureName
    }

}
