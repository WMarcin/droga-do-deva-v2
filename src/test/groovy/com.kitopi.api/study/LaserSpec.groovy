package com.kitopi.api.study

import com.kitopi.api.study.application.Laser.Laser
import spock.lang.Specification

class LaserSpec extends Specification {
    Laser laser = new Laser()

    def 'Charge the laser for 1 second and fire - the laser power should be 10'() {
        given:
            int loadTime = 1
        expect:
            laser.loadingLaser(loadTime) == 10
    }

    def 'Charge the laser for 10 second and fire - the laser power should be 100'() {
        given:
            int loadTime = 10
        when:
            int result = laser.loadingLaser(loadTime)
        then:
            result == 100
    }

    def 'Charge the laser for 20 second and fire - the laser power should be 20'() {
        given:
            int loadTime = 20
        when:
            int result = laser.loadingLaser(loadTime)
        then:
            result == 100
    }

    def 'Charge the laser for 2 seconds and fire. Reload the laser for 4 seconds and fire. After 2 shots, the laser power should be 40'() {
        given:
            int loadTime = 2
            int loadTime2 = 4
        when:
            int result = laser.loadingLaser(loadTime)
        and:
            int secondResult = laser.loadingLaser(loadTime2)
        then:
            result == 20
            secondResult == 40
    }

    def 'x'() {
        given:
            int loadTime = 2
            int loadTime2 = 4
        when:
            int result = laser.loadingLaser(loadTime)
        and:
            int secondResult = laser.loadingLaser(loadTime2)
        then:
            result == 20
            secondResult == 40
    }
}

