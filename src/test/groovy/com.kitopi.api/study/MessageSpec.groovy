package com.kitopi.api.study

import com.kitopi.api.study.application.Message.CurrentDateForTest
import com.kitopi.api.study.application.Message.Message
import spock.lang.Specification

class MessageSpec extends Specification {

    def 'Compose a new message'() {
        given:
            Date currentDate = new Date(2022, 5, 5)
            CurrentDateForTest dateProvider = new CurrentDateForTest();
            dateProvider.setCurrentDate(currentDate);
        when:
            Message fullMasage = Message.fullMessage("Wiadomość", "Marcin", dateProvider)
        then:
            fullMasage.getMessage() == "Wiadomość"
            fullMasage.getRecipient() == "Marcin"
            fullMasage.date == currentDate
    }
    def 'Changing message'() {
        given:
            Date currentDate = new Date(2022, 5, 5)
            CurrentDateForTest dateProvider = new CurrentDateForTest();
            dateProvider.setCurrentDate(currentDate);

            Message fullMasage = Message.fullMessage("Wiadomość", "Marcin", dateProvider)
        when:
            Date laterDate = new Date(2022, 5, 6)
            dateProvider.setCurrentDate(laterDate)
            Message fullMasageChanging = fullMasage.setMessage("Treść").setRecipient("Tomek")
        then:
            fullMasageChanging.getMessage() == "Treść"
            fullMasageChanging.getRecipient() == "Tomek"
            fullMasageChanging.getDate() == laterDate

            fullMasage.getDate() != fullMasageChanging.getDate()

            fullMasage.getMessage() == "Wiadomość"
            fullMasage.getRecipient() == "Marcin"
            fullMasage.getDate() == currentDate

    }
}