package com.kitopi.api.study

import com.kitopi.api.study.application.SureNames
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@AutoConfigureMockMvc
@WebMvcTest
class FullNameControllerSpec extends Specification {

    @Autowired
    MockMvc mvc

    @MockBean
    SureNames sureNames

    def "get full name"() {
        expect:
            mvc.perform(get("/api/getFullName"))
                    .andExpect(status().isOk())
                    .andReturn()
                    .response
                    .contentAsString == "Marcin Wrzeszcz"
    }

}
