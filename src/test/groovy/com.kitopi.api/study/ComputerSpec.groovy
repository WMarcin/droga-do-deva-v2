package com.kitopi.api.study


import com.kitopi.api.study.application.Computer.Item
import com.kitopi.api.study.application.Computer.Order
import spock.lang.Specification

class ComputerSpec extends Specification {

    private final static Item PROCESSOR = new Item("Intel Core i7", 2000)
    private final static Item RAM = new Item("DDR5", 1000)
    private final static Item DISC = new Item("1TB ", 500)

    def 'We add an Intel processor,RAM DDR5,disk 1TB. The total contract value should be 3500'() {
        given:
            Order order1 = Order.of()
            order1.addItem(PROCESSOR)
            order1.addItem(RAM)
            order1.addItem(DISC)
        when:
            double calculatePrice = order1.calculatePrice()

        then:
            calculatePrice == 3500
    }

    def 'the order cannot contain more than 5 items'() {
        given:
            Order order1 = Order.of()
            order1.addItem(PROCESSOR)
            order1.addItem(PROCESSOR)
            order1.addItem(DISC)
            order1.addItem(DISC)
            order1.addItem(DISC)

        when:
            order1.addItem(RAM)

        then:
            def e = thrown RuntimeException
            e.message == "Too many items"
    }

    def 'apply 10 discount on your order'() {
        given:
            Order order1 = Order.of()
            order1.addItem(PROCESSOR)
            order1.addItem(RAM)
            order1.addItem(DISC)

        when:
            double calculatePrice = order1.totalPrice()

        then:
            calculatePrice == 3150
    }
}
