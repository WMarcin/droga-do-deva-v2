package com.kitopi.api.study


import com.kitopi.api.study.application.Student.Exam
import com.kitopi.api.study.application.Student.Grade
import static org.assertj.core.api.Assertions.assertThat
import spock.lang.Specification

class StudentSpec extends Specification {

    private final static Grade STUDENT1 = Grade.of("001123", 1)
    private final static Grade STUDENT2 = Grade.of("112234", 3)
    private final static Grade STUDENT3 = Grade.of("223345", 4)
    private final static Grade STUDENT4 = Grade.of("334456",6)
    private final static Grade STUDENT2_COPIED = Grade.of("112234", 4)

    def 'The average grade should be 3.5'() {

        given:
            Exam exam1 = Exam.of()
            exam1.addGrade(STUDENT1)
            exam1.addGrade(STUDENT2)
            exam1.addGrade(STUDENT3)
            exam1.addGrade(STUDENT4)

        when:
            double averageGrade = exam1.averageGrade2()


        then:
            averageGrade == 3.5
    }

    def 'a student may only have one grade'() {

        given:
            Exam exam1 = Exam.of()
            exam1.addGrade(STUDENT1)
            exam1.addGrade(STUDENT2)
            exam1.addGrade(STUDENT3)
            exam1.addGrade(STUDENT4)

        when:
            exam1.addGrade(STUDENT2_COPIED)

        then:
            def e = thrown RuntimeException
            e.message == "a student may only have one grade"
    }

    def 'The failed list should include the student ID: 001123'() {

        given:
            Exam exam1 = Exam.of()
            exam1.addGrade(STUDENT1)
            exam1.addGrade(STUDENT2)
            exam1.addGrade(STUDENT3)
            exam1.addGrade(STUDENT4)

        when:
            List<Grade> failed = exam1.failedExam()

        then:
            assertThat(failed).containsExactly(STUDENT1)
    }

    def 'The failed list should be empty'() {

        given:
            Exam exam1 = Exam.of()
            exam1.addGrade(STUDENT2)
            exam1.addGrade(STUDENT3)
            exam1.addGrade(STUDENT4)

        when:
            List<Grade> failed = exam1.failedExam()

        then:
            failed == []
    }
}