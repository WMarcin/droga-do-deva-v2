package com.kitopi.api.study

import com.kitopi.api.study.application.Laser.Laser
import com.kitopi.api.study.application.immutability.StarDestroyer
import spock.lang.Specification

class ImutableStarDestroyer extends Specification{

    def 'Charging immutable star destroyer creates new instance'(){
        given:
            StarDestroyer starDestroyer = new StarDestroyer("Starek");
        when:
            StarDestroyer newStarDestroyer = starDestroyer.loadLaser(1)
        then:
            starDestroyer.getName() == "Starek"
            newStarDestroyer.getName() == "Starek"
            starDestroyer.getCurrentPower() == 0
            newStarDestroyer.getCurrentPower() == 10
    }

    def 'Charging immutable star destroyer creates new instance and sum up with every loading process'(){
        given:
            StarDestroyer starDestroyer = new StarDestroyer("Starek");
        when:
            StarDestroyer starDestroyerV2 = starDestroyer.loadLaser(1)
            StarDestroyer starDestroyerV3 = starDestroyerV2.loadLaser(1)
        then:
            starDestroyer.getCurrentPower() == 0
            starDestroyerV2.getCurrentPower() == 10
            starDestroyerV3.getCurrentPower() == 20
    }

    def 'Charging immutable star destroyer is shooting'(){
        given:
            StarDestroyer starDestroyer = new StarDestroyer("Starek");
        when:
            starDestroyer = starDestroyer.loadLaser(5)
            StarDestroyer newStarDestroyer = starDestroyer.fire()
        then:
            starDestroyer.getName() == "Starek"
            newStarDestroyer.getName() == "Starek"
            starDestroyer.getCurrentPower() == 50
            newStarDestroyer.getCurrentPower() == 0
    }

    def 'Charging immutable star destroyer is shooting 2'(){
        given:
            StarDestroyer starDestroyer = new StarDestroyer("Starek");


        when:
            starDestroyer = starDestroyer
                    .loadLaser(5)
                    .fire()

        then:
            starDestroyer.getName() == "Starek"
            starDestroyer.getCurrentPower() == 0
    }
    

}

