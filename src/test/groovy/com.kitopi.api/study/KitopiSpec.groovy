package com.kitopi.api.study

import com.kitopi.api.study.application.Kitopi.MenuPosition
import com.kitopi.api.study.application.Kitopi.Order
import com.kitopi.api.study.application.Kitopi.Percentage30Discount
import com.kitopi.api.study.application.Kitopi.PercentageDiscount
import com.kitopi.api.study.application.Kitopi.QuantitativeDiscount
import spock.lang.Specification

class KitopiSpec extends Specification {

    private final static MenuPosition CHEESEBURGER = new MenuPosition("DubleChees", 3)
    private final static MenuPosition SODA = new MenuPosition("Water", 1)
    private final static MenuPosition FRIES = new MenuPosition("Fries", 1.5)

    def 'Adding two cheeseburgers, one fries, and one fizzy drink will cost you 8.50'() {
        given:
        Order order1 = Order.of()
        order1.addMenuPosition(CHEESEBURGER)
        order1.addMenuPosition(CHEESEBURGER)
        order1.addMenuPosition(SODA)
        order1.addMenuPosition(FRIES)
        when:
            double calculatePrice = order1.calculatePrice()


        then:
            calculatePr ice == 8.5
    }

    def 'the order cannot contain more than 10 items'() {
        given:
            Order order1 = Order.of()
            for (int i = 0; i < 10; i++) {
                order1.addMenuPosition(CHEESEBURGER)
            }

        when:
            order1.addMenuPosition(SODA)


        then:
            def e = thrown RuntimeException
            e.message == "Too many items"
    }

    def 'the order cannot cost more than 15'() {
        given:
            Order order1 = Order.of()
            order1.addMenuPosition(CHEESEBURGER)
            order1.addMenuPosition(CHEESEBURGER)
            order1.addMenuPosition(CHEESEBURGER)
            order1.addMenuPosition(CHEESEBURGER)
            order1.addMenuPosition(CHEESEBURGER)


        when:
            order1.addMenuPosition(SODA)


        then:
            def e = thrown RuntimeException
            e.message == "Maximum price exceeded"
    }

    def 'apply 30% discount on your order'() {
        given:
            Order order1 = Order.of()
            order1.addMenuPosition(CHEESEBURGER)
            order1.addMenuPosition(CHEESEBURGER)

        when:
            double discount = order1.calculatePrice(new Percentage30Discount())

        then:
            discount == 6 - 6 * 30 / 100
    }

    def 'apply 10 discount on your order'() {
        given:
            Order order1 = Order.of()
            order1.addMenuPosition(CHEESEBURGER)
            order1.addMenuPosition(CHEESEBURGER)
            order1.addMenuPosition(CHEESEBURGER)
            order1.addMenuPosition(CHEESEBURGER)

        when:
            double discount = order1.calculatePrice(new QuantitativeDiscount())


        then:
            discount == 2
    }

    def 'apply 50 discount on your order'() {
        given:
            Order order1 = Order.of()
            order1.addMenuPosition(CHEESEBURGER)
            order1.addMenuPosition(CHEESEBURGER)
            order1.addMenuPosition(CHEESEBURGER)
            order1.addMenuPosition(CHEESEBURGER)

        when:
            double discount = order1.calculatePrice(new PercentageDiscount(50))


        then:
            discount == 6
    }
}
