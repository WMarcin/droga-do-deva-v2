package com.kitopi.api.study

import com.kitopi.api.study.application.Aeroplane.Aeroplane
import spock.lang.Specification

class AeroplaneSpec extends Specification{


    def 'Should add fuel to plane'(){
        given:
            Aeroplane plane = new Aeroplane(1000)
        when:
            plane.addFuel(100)
        then:
            plane.getCurrentFuel() == 100
    }

    def 'Should add fuel to plane but cannot exceed capacity'(){
        given:
            Aeroplane plane = new Aeroplane(1000)
        when:
            plane.addFuel(10000)
        then:
            def e = thrown RuntimeException
            e.message == "Fuel level cannot be exceeded above 1000"

            plane.getCurrentFuel() == 0
    }

    def 'Plane cannot take off with empty fuel tank'(){
        given:
            Aeroplane plane = new Aeroplane(1000)
        when:
            plane.takeOff()
        then:
            def e = thrown RuntimeException
            e.message == "Cannot take off - not enought fuel"
            plane.isFlying() == false
    }

    def 'Plane cannot take off when flying already'(){
        given:
            Aeroplane plane = new Aeroplane(1000)
            plane.addFuel(100)
            plane.takeOff()
        when:
            plane.takeOff()
        then:
            def e = thrown RuntimeException
            e.message == "Plane is already flying"
            plane.isFlying() == true
    }

    def 'Plane can take off if has enought fuel'(){
        given:
            Aeroplane plane = new Aeroplane(1000)
            plane.addFuel(100)
        when:
            plane.takeOff()
        then:
            plane.isFlying() == true
    }

    def 'Plane should be able to land after correct take off'(){
        given:
          Aeroplane plane = new Aeroplane(1000)
            plane.addFuel(100)
            plane.takeOff()
        when:
            plane.land()
        then:
            plane.isFlying() == false

    }

    def 'Plane should not be able to land if is already on the ground'(){
        given:
            Aeroplane plane = new Aeroplane(1000)
            plane.addFuel(100)
            plane.takeOff()
            plane.land()
        when:
            plane.land()
        then:
            def e = thrown RuntimeException
            e.message == "Plane is already on the ground"
            plane.isFlying() == false
    }
    def 'Plane should be able to fly for 90 minutes'(){
        given:
            Aeroplane plane = new Aeroplane(1000)
            plane.addFuel(1000)
            plane.takeOff()
        when:
            plane.fly(90)
        then:
            plane.currentFuel == 100
    }
    def 'Plane should not be able to fly for 150 minutes'(){
        given:
            Aeroplane plane = new Aeroplane(1000)
            plane.addFuel(1000)
            plane.takeOff()
        when:
            plane.fly(150)
        then:
            def e = thrown RuntimeException
            e.message == "Not enough fuel to reach your destination"
            plane.currentFuel == 1000
    }
    def 'Plane should not be able to fly for 10 minutes if not started'(){
        given:
            Aeroplane plane = new Aeroplane(1000)
            plane.addFuel(1000)
        when:
            plane.fly(10)
        then:
            def e = thrown RuntimeException
            e.message == "Plane is not able to fly, not started yet"
            plane.isFlying() == false
    }
}

