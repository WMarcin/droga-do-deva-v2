package com.kitopi.api.study

import com.kitopi.api.study.application.Laser.Laser
import spock.lang.Specification

class StringSpec extends Specification{
    Laser laser = new Laser()

    def 'String basic test'(){
        given:
            String test = "test"
        when:
            test = test.concat("test2")
        then:
        test == "testtest2"

    }

    def 'String basic test 2'(){
        given:
            String test = "test"
        when:
            String test2 = test.concat("test2")
        then:
            test == "test"
            test2 == "testtest2"
    }

    def 'String fun'(){
        given:
            String a = "test"
        when:
            String b = a.concat('-test2').concat("-test3").toUpperCase()
        then:
            a == "test"
            b == "TEST-TEST2-TEST3"
    }

}

